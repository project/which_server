<?php

/**
 * @file
 * The main whichserver module file.
 */

/**
 * Implements hook_mail_alter().
 *
 * Add server name to update module's notification email message.
 */
function whichserver_mail_alter(&$message) {
  if ($message['id'] == 'update_status_notify') {
    $servername = _whichserver_get_servername();
    $shortname = substr($servername, 0, strpos($servername, '.'));
    $message['body']['whichserver_servername'] = t('Server: !s', array(
      '!s' => $servername
    ));
    $message['subject'] = '[' . $shortname . '] ' . $message['subject'];
    module_load_include('inc', 'update', 'update.compare');
    $data = update_calculate_project_data(TRUE);
    $out = "Modules needing attention:\n\n";
    foreach ($data as $module_info) {
      if ($module_info['status'] != UPDATE_CURRENT) {
        $out .= ' * ' . $module_info['title'] . ' (' . $module_info['name'] . ') ';
        $params = array(
          '!e' => $module_info['existing_version'],
          '!f' => $module_info['recommended']
        );
        switch ($module_info['status']) {
          case UPDATE_NOT_SECURE:
            $out .= t(' - SECURITY UPDATE: !e to !f', $params);
            break;
          case UPDATE_NOT_CURRENT:
            $out .= t(' - update available: !e to !f', $params);
            break;
          case UPDATE_NOT_SUPPORTED:
            $out .= t(' - version no longer supported: !e', $params);
            break;
          case UPDATE_REVOKED:
            $out .= t(' - version no longer available: !e', $params);
            break;
        }
        $out .= "\n";
      }
    }
    $message['body']['list'] = $out;
  }
}

/**
 * Implements hook_help().
 */
function whichserver_help($path, $arg) {
  switch ($path) {
    case 'admin/help#whichserver':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The Which Server module that adds the server name to a Drupal site’s "Available updates" page, and to the update notification emails sent by Drupal.') . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<p>' . t('No configuration is required. When this module is enabled, the server name will now appear here:') . '</p>';
      $output .= '<ul>';
      $output .= '<li><a href="/admin/reports/status">' . t('Status Report page') . '</a></li>';
      $output .= '<li><a href="/admin/modules">' . t('Modules page') . '</a></li>';
      $output .= '</ul>';
      $output .= '<p>' . t('and also, if the core Update Manager module is enabled, here:') . '</p>';
      $output .= '<ul><li><a href="/admin/reports/updates">' . t('Available Updates page') . '</a></li>';
      $output .= '<li>' . t('Available Updates email notifications') . ' <a href="/admin/reports/updates/settings">' . t('(configured here)') . '</a></li></ul>';
      return $output;
      break;

    case 'admin/reports/updates':
    case 'admin/modules':
      return "<p>" . t('Server') . ': <strong>' . _whichserver_get_servername() . '</strong></p>';
      break;
  }
}

/**
 * Implements hook_admin_paths().
 *
 * Open help in admin overlay.
 */
function whichserver_admin_paths() {
  $paths = array(
    'help/whichserver/*' => TRUE,
  );
  return $paths;
}

/**
 * Return a string containing the server's name.
 *
 * Note: using $_SERVER['SERVER_NAME'] is no good as this
 * only returns the VirtualHost name.
 */
function _whichserver_get_servername() {
  $servername = php_uname('n');
  return $servername;
}
