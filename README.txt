INTRODUCTION
------------

This is a simple module that adds the server name to a Drupal site's
"Available updates" page, and to the update notification emails sent by Drupal.
This makes it easier for a Drupal site maintainer to see which of several
servers the site is installed on.


INSTALLATION & CONFIGURATION
----------------------------

1) Install module and enable it.

That's it, nothing else needed.


USAGE
-----

Server name will now appear here:

 * Status Report page
 * Modules page

and also, if the core Update Manager module is enabled, here:

 * Available Updates page
 * Available Updates email notifications

 
CONTACT
-------

This module was written by, and is maintained by,
Anthony Cartmell <ajcartmell@fonant.com>
